<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'Law-divi' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'cb&;d@H++IpFRq:L@-(IH0,_%<Y,<&%$zHpV|Jla>!G+1uyY4*&-Y{/N0<fbAN{v' );
define( 'SECURE_AUTH_KEY',  'e{(Hl<2733-NP0L!y2l<Qb1!/rFRzBaw8WM23Z&q>g>S_[se4<t~-]&/0zJ!.6gv' );
define( 'LOGGED_IN_KEY',    '{X+wt4{2t.K<H)Hl7X!5J5/g2 qC23VrIbCi;Uz@+nT?=[q/.c&ZALl5K=HS|g&P' );
define( 'NONCE_KEY',        '[Y&PN>&4sHKP%3[zlm%L@=`CY}AgtZ{c5DUUk`V;#UPW|E%TI(y,E_ bwQiiND*f' );
define( 'AUTH_SALT',        '@<zJt6M^ k4*@(lj+hWj/{*`7J@RdB}}1PEz9m9Cj6W4t@VteX80>C#OnYC+U{WR' );
define( 'SECURE_AUTH_SALT', '&xY&or{z)>|:6VS8WV_!cU*:prZonX>p{nVmf<PlfNQ?TW8PoYL9AZ9[d|6zB!]c' );
define( 'LOGGED_IN_SALT',   'A$fW.uT[0$T.XRYfWlNhb%0$u(0dIt=/?9m9u~N;HNu<AGN=]qhU)R>Pylo6;z*$' );
define( 'NONCE_SALT',       'P{-&l=v3&$v!qorRDnZ7d.SWXZU9.YXKuSqPqz_TLi5nH3qCI;k_t/JLQQJppemy' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
